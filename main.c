#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>

//               0   1    2    3   4   5    6    7    8    9  . 
int codes[] = {222, 10, 230, 110, 58, 124, 252, 14, 254, 126, 1};

volatile int timer = 0;

void transfer(int num) {
        SPDR = num;
        while (!(SPSR&(1<<SPIF)));
        PORTB |= (1<<PORTB2);
        PORTB &= ~(1<<PORTB2);
}

void segment_number(int num) {
        transfer(codes[num]);
}

ISR(INT0_vect) { 
        timer = 0;
}

ISR(TIMER1_COMPA_vect) {
        if(timer > 9) timer = 0;
        timer ++;
}

void setup() {
	DDRB |= ((1<<PORTB2)|(1<<PORTB3)|(1<<PORTB5));
	PORTB &= ~((1<<PORTB2)|(1<<PORTB3)|(1<<PORTB5));
	SPCR = ((1<<SPE)|(1<<MSTR));
	SPDR = 0b00000000;
	while (!(SPSR&(1<<SPIF)));
	PORTB |= (1<<PORTB2);
	PORTB &= ~(1<<PORTB2);
        _delay_ms(1000);

        cli();
        TCCR1A = 0;
        TCCR1B = 0;
        OCR1A = 15624;
        TCCR1B |= (1 << WGM12);
        TCCR1B |= (1 << CS10);
        TCCR1B |= (1 << CS12);
        TIMSK1 |= (1 << OCIE1A);

        DDRD  = 0b00010000;
        PORTD = 0b00000100;

        EICRA |= (1<<ISC01);
        EIMSK |= (1<<INT0);

        sei();
}

int main() { 
	setup();
       	while (1) {
		segment_number(timer);
	}
       	return 0;
}

