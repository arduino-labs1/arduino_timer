#include "SPI.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//               0   1    2    3   4   5    6    7    8    9  . 
int codes[] = {222, 10, 230, 110, 58, 124, 252, 14, 254, 126, 1};

const int pinSelect = 8;
volatile int timer = 0;

void segment_number (int num) {
        digitalWrite(pinSelect, LOW);
        SPI.transfer(codes[num]);
        digitalWrite(pinSelect, HIGH);
}

ISR(INT0_vect) {
        timer = 0;
        segment_number(timer);
}

ISR (TIMER1_COMPA_vect) {
        if(timer > 9) timer = 0;
        segment_number(timer);
        timer ++;
}

void setup() {
        SPI.begin();
        pinMode(pinSelect, OUTPUT);
        digitalWrite(pinSelect, LOW);
        SPI.transfer(0);
        digitalWrite(pinSelect, HIGH);
        Serial.begin(9600);

        cli();
        TCCR1A = 0;
        TCCR1B = 0;
        OCR1A = 15624;
        TCCR1B |= (1 << WGM12);
        TCCR1B |= (1 << CS10);
        TCCR1B |= (1 << CS12);
        TIMSK1 |= (1 << OCIE1A);

        DDRD  = 0b00010000;
        PORTD = 0b00000100;

        EICRA |= (1<<ISC01);
        EIMSK |= (1<<INT0);

        sei();
}

void loop() {}
