# arduino_timer

Timer with Arduino Nano and SN74HC595N

There are an addition function: reset at the touch of a button.

## Content
* Arduino Nano
* Shift Register SN74HC595N
* Seven-segment display
* Button
* Resistor  x8
* Connecting wires

## Run
```bash
$ avr-gcc -g -O1 -mmcu=atmega328p -o main.elf main.c
$ avrdude -v -patmega328p -c arduino -P /dev/ttyUSB0 -U flash:w:'main.elf'
```
